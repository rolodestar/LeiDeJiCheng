#include "stdafx.h"
#include "base.h"


base::base()
	:a(10),b(5)
{
}

base::~base()
{
	throw gcnew System::NotImplementedException();
}

int base::add()
{
	cout << "in the base -- add fuc" << endl;
	return a+b;
}

int base::del()
{
	cout << "in the base -- del fuc" << endl;
	return a-b;
}

int base::virFuc()
{
	cout << "in the base -- virFuc fuc" << endl;
	return a+b;
}
